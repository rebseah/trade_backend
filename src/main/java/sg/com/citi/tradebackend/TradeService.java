package sg.com.citi.tradebackend;

import sg.com.citi.tradebackend.model.Trade;
import sg.com.citi.tradebackend.model.TradeState;
import sg.com.citi.tradebackend.model.TradeType;

import java.util.List;

public interface TradeService {

    List<Trade> getAllTradeRecords();
    List<Trade> getTradesByTradeType(TradeType tradeType);
    List<Trade> getTradesByTradeState(TradeState tradeState);
    void addTradeRecord(Trade trade);
    void updateTradeRecordById(int id, Trade trade);
    void deleteTradeRecord(int id);
}