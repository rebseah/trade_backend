package sg.com.citi.tradebackend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sg.com.citi.tradebackend.model.Trade;
import sg.com.citi.tradebackend.model.TradeState;
import sg.com.citi.tradebackend.model.TradeType;

import java.util.List;

@Service
public class TradeServiceImpl implements TradeService{

    @Autowired
    private TradeRepository tradeRepository;


    @Override
    public List<Trade> getAllTradeRecords() {
        List<Trade> results = tradeRepository.findAll();
        return results;
    }

    @Override
    public List<Trade> getTradesByTradeType(TradeType tradeType) {
        List<Trade> result = tradeRepository.findByType(tradeType);
        return result;
    }

    @Override
    public List<Trade> getTradesByTradeState(TradeState tradeState) {
        List<Trade> result = tradeRepository.findByState(tradeState);
        return result;
    }

    @Override
    public void addTradeRecord(Trade tradeRecord) {
        tradeRepository.save(tradeRecord);
    }

    @Override
    public void updateTradeRecordById(int id, Trade trade) {
        Trade oldTrade = tradeRepository.findById(id).get();

        TradeState tradeState = TradeState.valueOf(trade.getState().getState().toUpperCase());
        TradeType tradeType = TradeType.valueOf(trade.getType().getTradeType().toUpperCase());

        oldTrade.setState(tradeState);
        oldTrade.setType(tradeType);
        oldTrade.setTicker(trade.getTicker());
        oldTrade.setQuantity(trade.getQuantity());
        oldTrade.setPrice(trade.getPrice());
        tradeRepository.save(oldTrade);
    }

    @Override
    public void deleteTradeRecord(int id) {
        Trade trade = tradeRepository.findById(id).get();
        tradeRepository.delete(trade);
    }


}
